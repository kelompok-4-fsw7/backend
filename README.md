# Backend Secondhand

## User Credentials

- Email : ryanadhitama2@gmail.com
- Password : rahasia123

## Tools

- Visual Studio Code

## Extension Visual Studio Code

- Prettier - Code formatter
- GitLens — Git supercharged
- EditorConfig for VS Code

## Cara Menjalankan

1. Clone repository ini dengan menggunakan perintah `git clone https://{username_gitlab}@gitlab.com/kelompok-4-fsw7/backend.git` pada terminal / command line.
2. Pastikan folder project sudah aktif pada terminal / command line.
3. Jalankan perintah `yarn install`
4. Jalankan perintah `yarn prepare`
5. Jalankan perintah `yarn start`
6. Jalankan perintah `yarn test` untuk melakukan testing (optional)
7. Happy Hacking

## List Endpoint

| Route                    | Method   | Penggunaan                                             |
| ------------------------ | -------- | ------------------------------------------------------ |
| /auth/login              | `POST`   | Melakukan proses login                                 |
| /auth/register           | `POST`   | Melakukan proses register                              |
| /auth/confirm            | `POST`   | Melakukan proses konfirmasi akun                       |
| /auth/resendEmail        | `POST`   | Mengirim token konfirmasi kepada user                  |
| /auth/password/forget    | `POST`   | Mengirim link lupa password kepada user                |
| /auth/password/reset     | `POST`   | Melakukan proses reset password                        |
| /auth/profile            | `PUT`    | Mengupdate data user yang sedang login                 |
| /auth/password           | `PUT`    | Mengupdate password user saat login                    |
| /auth/whoami             | `GET`    | Mendapatkan data user yang sedang login                |
| /categories              | `GET`    | Mendapatkan data kategori produk                       |
| /cities                  | `GET`    | Mendapatkan data kota                                  |
| /products                | `GET`    | Mendapatkan data list produk                           |
| /products                | `POST`   | Menyimpan data produk baru                             |
| /products/:id            | `PUT`    | Mengupdate data produk berdasar ID                     |
| /products/:id            | `DELETE` | Menghapus data produk berdasar ID                      |
| /products/:slug          | `GET`    | Mendapatkan data produk berdasar slug                  |
| /user/products           | `GET`    | Mendapatkan data produk berdasar pemilik               |
| /user/products/:id       | `GET`    | Mendapatkan data produk berdasar pemilik dan ID produk |
| /user/products-sold      | `GET`    | Mendapatkan data produk yang telah terjual             |
| /user/products-not-sold  | `GET`    | Mendapatkan data produk yang belum terjual             |
| /user/products-liked     | `GET`    | Mendapatkan data produk berdasar pemilik dan ID produk |
| /user/products-count     | `GET`    | Mendapatkan jumlah produk berdasar pemilik             |
| /product/:id/like        | `GET`    | Mendapatkan status like pada produk berdasar ID        |
| /product/:id/like        | `POST`   | Melakukan proses like pada produk berdasar ID          |
| /product/:id/offer       | `GET`    | Mendapatkan Status Offer pada produk berdasar ID       |
| /product/:id/offer       | `POST`   | Melakukan penawaran pada produk berdasar ID            |
| /product/:id/offer       | `PUT`    | Mengupdate Status Offer pada produk berdasar ID        |
| /user/offer/received     | `GET`    | Mendapatkan data list Offer yang telah diterima        |
| /user/offer/made         | `GET`    | Mendapatkan data list Offer yang telah dibuat          |
| /user/offer/:id          | `GET`    | Mendapatkan data Offer berdasar ID                     |
| /product/:id/offer/list  | `GET`    | Mendapatkan data Offer berdasar produk ID              |
| /product/:id/offer/:oid  | `PUT`    | Mengupdate status produk berdasar produk dan offer ID  |
| /notifications           | `GET`    | Mendapatkan data notifikasi berdasar user ID           |
| /notifications/:id       | `PUT`    | Mengupdate data notifikasi berdasar ID                 |
| /upload                  | `POST`   | Melakukan proses upload gambar                         |
| /user/transaction/seller | `GET`    | Mendapatkan data transaksi penjual                     |
| /user/transaction/buyer  | `GET`    | Mendapatkan data transaksi pembeli                     |

## Swagger

| Route     | Method | Penggunaan                            |
| --------- | ------ | ------------------------------------- |
| /docs     | `GET`  | Menampilkan swagger UI                |
| /api-docs | `GET`  | Menampilkan swagger dalam bentuk JSON |

## ERD

![Entity Relationship Diagram](https://res.cloudinary.com/dghuawjlr/image/upload/v1658653115/Untitled_5_aywagr.png)
