'use strict';
const slugify = require('slugify');

module.exports = {
  async up(queryInterface, Sequelize) {
    const timestamp = new Date();
    const products = [
      {
        category_id: 1,
        user_id: 2,
        name: 'Jam Tangan Rolex',
        slug: slugify('Jam Tangan Rolex', { lower: true }),
        price: 2800000,
        description:
          'No minus need to declare. Barang simpanan, jual santai cari harga baik. Milik pribadi dan jarang sekali dipakai.',
        status: 1,
        created_at: timestamp,
        updated_at: timestamp
      },
      {
        category_id: 2,
        user_id: 1,
        name: 'Yamaha Vixion 2014',
        slug: slugify('Yamaha Vixion 2014', { lower: true }),
        price: 8300000,
        description:
          'Bismillah...Dijual yamaha vixion 2014 Warna asli merah maroon, ditutup skotlet Mesin kering halus. Plat sampai 2024 Harga nego halus sehalus mesinya',
        status: 1,
        created_at: timestamp,
        updated_at: timestamp
      },
      {
        category_id: 2,
        user_id: 1,
        name: 'Yamaha NMax Non ABS 2018',
        slug: slugify('Yamaha NMax Non ABS 2018', { lower: true }),
        price: 22300000,
        description: '(DP 800 Rb) Plat B DKI JakBar NMax Non ABS Putih 2018 Cash & Kredit',
        status: 1,
        created_at: timestamp,
        updated_at: timestamp
      },
      {
        category_id: 3,
        user_id: 3,
        name: 'Samsung J2 Prime',
        slug: slugify('Samsung J2 Prime', { lower: true }),
        price: 550000,
        description: 'Kondisi Normal no minus, nego tipis aja',
        status: 1,
        created_at: timestamp,
        updated_at: timestamp
      },
      {
        category_id: 3,
        user_id: 4,
        name: 'Nokia 3310',
        slug: slugify('Nokia 3310', { lower: true }),
        price: 400000,
        description: 'Nokia Legend kondisi normal, HP + Charger',
        status: 1,
        created_at: timestamp,
        updated_at: timestamp
      }
    ];

    await queryInterface.bulkInsert('products', products, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     */
    await queryInterface.bulkDelete('products', null, {});
  }
};
