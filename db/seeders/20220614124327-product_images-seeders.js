'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    const timestamp = new Date();
    const product_images = [
      {
        product_id: 1,
        url: 'https://apollo-singapore.akamaized.net/v1/files/853935y19fet2-ID/image;s=780x0;q=60',
        created_at: timestamp,
        updated_at: timestamp
      },
      {
        product_id: 2,
        url: 'https://apollo-singapore.akamaized.net/v1/files/yb09w6jmr31i1-ID/image;s=780x0;q=60',
        created_at: timestamp,
        updated_at: timestamp
      },
      {
        product_id: 3,
        url: 'https://apollo-singapore.akamaized.net/v1/files/oksvt9t7a89f-ID/image;s=780x0;q=60',
        created_at: timestamp,
        updated_at: timestamp
      },
      {
        product_id: 4,
        url: 'https://apollo-singapore.akamaized.net/v1/files/fkapqgtgjljl1-ID/image;s=780x0;q=60',
        created_at: timestamp,
        updated_at: timestamp
      },
      {
        product_id: 5,
        url: 'https://apollo-singapore.akamaized.net/v1/files/6y90fud62jpi3-ID/image;s=780x0;q=60',
        created_at: timestamp,
        updated_at: timestamp
      }
    ];

    await queryInterface.bulkInsert('product_images', product_images, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('product_images', null, {});
  }
};
