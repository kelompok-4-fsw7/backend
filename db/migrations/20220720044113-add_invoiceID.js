'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn('transactions', 'invoice_id', { type: Sequelize.STRING });
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn('transactions', 'invoice_id');
  }
};
