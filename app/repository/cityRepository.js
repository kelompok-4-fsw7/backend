const { City } = require('../models');

class CityRepository {
  async findAll() {
    const cities = await City.findAll();
    return cities;
  }
}

module.exports = new CityRepository();
