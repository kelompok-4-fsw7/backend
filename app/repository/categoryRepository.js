const { Category } = require('../models');

class CategoryRepository {
  async findAll() {
    const category = await Category.findAll();
    return category;
  }
}

module.exports = new CategoryRepository();
