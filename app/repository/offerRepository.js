const { Offers, Products, User, ProductImage } = require('../models');

class offerRepository {
  async create(id, reqBody, product_id, seller_id) {
    const { offer_price } = reqBody;
    const offer = await Offers.create({
      user_id: id,
      offer_price,
      product_id,
      status: 1,
      seller_id
    });
    return offer;
  }

  async update(offer_id, status) {
    try {
      const result = await Offers.update(status, {
        where: {
          id: offer_id
        }
      });
      return result;
    } catch (err) {
      console.log(err);
    }
  }
  async rejectOffer(product_id) {
    try {
      const result = await Offers.update(
        {
          status: 0
        },
        {
          where: {
            product_id
          }
        }
      );
      return result;
    } catch (err) {
      console.log(err);
    }
  }
  async getOfferStatusByProductUser(user_id, product_id) {
    const offer = await Offers.findOne({
      where: {
        user_id,
        product_id,
        status: 1
      }
    });
    return offer;
  }

  getOfferByProductID = async (product_id) => {
    const offer = await Offers.findOne({
      where: {
        product_id
      },
      include: [
        { model: User, as: 'user' },
        { model: Products, as: 'product' }
      ]
    });

    return offer;
  };

  getOfferByID = async (id) => {
    const offer = await Offers.findOne({
      where: {
        id
      },
      include: [
        { model: User, as: 'user' },
        { model: Products, as: 'product', include: { model: ProductImage, as: 'images' } }
      ]
    });

    return offer;
  };

  getOfferBySellerID = async (seller_id) => {
    const offer = await Offers.findAll({
      where: {
        seller_id
      },
      include: [
        { model: User, as: 'user' },
        { model: Products, as: 'product', include: { model: ProductImage, as: 'images' } }
      ],
      order: [['created_at', 'DESC']]
    });

    return offer;
  };

  getOfferByUserID = async (user_id) => {
    const offer = await Offers.findAll({
      where: {
        user_id
      },
      include: [
        { model: User, as: 'user' },
        { model: Products, as: 'product', include: { model: ProductImage, as: 'images' } }
      ],
      order: [['created_at', 'DESC']]
    });

    return offer;
  };
}

module.exports = new offerRepository();
