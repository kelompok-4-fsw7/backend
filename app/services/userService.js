const UserRepository = require('../repository/userRepository');
const {
  EmailNotRegisteredError,
  EmailAlreadyTakenError,
  WrongPasswordError,
  UserInactive,
  TokenInvalidError,
  UserAlreadyActiveError,
  UnprocessableEntityError
} = require('../errors');
const { verifyPassword, encryptPassword } = require('../libs/password');
const { createToken, getRandomToken } = require('../libs/token');
const { sendEmail } = require('../libs/email');

class UserService {
  register = async ({ reqBody, response }) => {
    const email = reqBody.email.toLowerCase();
    const randomToken = getRandomToken();
    const existingUser = await UserRepository.findByEmail(email);
    const content = `https://secondhand.vercel.app/confirm/${randomToken}?email=${email}`;
    if (existingUser) {
      const err = new EmailAlreadyTakenError(reqBody.email);
      response.status(422).json(err);
      return;
    }
    sendEmail(email, 'confirmation', content);
    const user = await UserRepository.createUser(reqBody, randomToken);
    const accessToken = createToken(user);
    return accessToken;
  };

  update = async ({ id, newData }, response) => {
    const { password } = newData;
    if (password) {
      const err = new UnprocessableEntityError();
      return response.status(422).json(err);
    }
    const user = await UserRepository.updateUser({ newData, id });
    if (user) {
      return user;
    }
  };

  updatePassword = async ({ email, newData, response }) => {
    const oldPassword = newData.oldPassword;
    const newPassword = newData.newPassword;

    const existingUser = await UserRepository.findByEmail(email);

    const isPasswordCorrect = verifyPassword(oldPassword, existingUser.password);
    if (!isPasswordCorrect) {
      const err = new WrongPasswordError();
      response.status(401).json(err);
      return;
    }

    const data = {
      password: encryptPassword(newPassword)
    };

    const user = await UserRepository.updateUser({ newData: data, id: existingUser.id });
    return user;
  };

  login = async ({ reqBody, response }) => {
    const email = reqBody.email.toLowerCase();
    const password = reqBody.password;

    const user = await UserRepository.findByEmail(email);

    if (!user) {
      const err = new EmailNotRegisteredError(email);
      return response.status(404).json(err);
    }

    if (user.status < 1) {
      const err = new UserInactive(user);
      return response.status(403).json(err);
    }
    const isPasswordCorrect = verifyPassword(password, user.password);

    if (!isPasswordCorrect) {
      const err = new WrongPasswordError();
      response.status(401).json(err);
      return;
    }

    const accessToken = createToken(user);
    return accessToken;
  };

  confirm = async ({ reqBody, response }) => {
    const email = reqBody.email.toLowerCase();
    const token = reqBody.token;
    const user = await UserRepository.findByEmail(email);

    if (!user) {
      const err = new EmailNotRegisteredError(email);
      return response.status(404).json(err);
    }

    if (user.status >= 1) {
      const err = new UserAlreadyActiveError(email);
      response.status(400).json(err);
      return;
    }

    if (user.token != token) {
      const err = new TokenInvalidError();
      response.status(400).json(err);
      return;
    }
    const updateUserStatus = await UserRepository.updateStatusUser(email);
    return updateUserStatus;
  };

  resendEmail = async ({ reqBody, response }) => {
    const email = reqBody.email.toLowerCase();
    const randomToken = await getRandomToken();
    const user = await UserRepository.findByEmail(email);

    if (!user) {
      const err = new EmailNotRegisteredError(email);
      return response.status(404).json(err);
    }

    if (user.status >= 1) {
      const err = new UserAlreadyActiveError(email);
      response.status(400).json(err);
      return;
    }

    const updateToken = await UserRepository.updateUserToken(randomToken, email);
    const content = `https://secondhand.vercel.app/confirm/${randomToken}?email=${email}`;
    sendEmail(email, 'confirmation', content);

    return updateToken;
  };

  forgetPassword = async ({ reqBody, response }) => {
    const email = reqBody.email.toLowerCase();
    const randomToken = await getRandomToken();
    const user = await UserRepository.findByEmail(email);

    if (!user) {
      const err = new EmailNotRegisteredError(email);
      return response.status(404).json(err);
    }

    if (user.status < 1) {
      const err = new UserInactive(user);
      return response.status(403).json(err);
    }
    const updateToken = await UserRepository.updateUserToken(randomToken, email);
    const content = `https://secondhand.vercel.app/reset-password?token=${randomToken}&email=${email}`;
    sendEmail(email, 'password', content);

    return updateToken;
  };

  resetPassword = async ({ reqBody, response }) => {
    const email = reqBody.email.toLowerCase();
    const token = reqBody.token;
    const newPassword = reqBody.newPassword;

    const user = await UserRepository.findByEmail(email);

    if (!user) {
      const err = new EmailNotRegisteredError(email);
      return response.status(404).json(err);
    }

    if (user.token != token) {
      const err = new TokenInvalidError();
      return response.status(401).json(err);
    }

    const data = {
      password: encryptPassword(newPassword),
      token: null
    };

    const updateUser = await UserRepository.updateUser({ newData: data, id: user.id });
    return updateUser;
  };
}

module.exports = new UserService();
