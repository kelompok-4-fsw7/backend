const CategoryRepository = require('../repository/categoryRepository');

class CategoryService {
  list = async () => {
    const category = await CategoryRepository.findAll();
    return category;
  };
}

module.exports = new CategoryService();
