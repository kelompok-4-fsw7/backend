const { Op } = require('sequelize');
const ProductRepository = require('../repository/productRepository');
const ProductImageRepository = require('../repository/productImageRepository');
const ProductLikeRepository = require('../repository/productLikeRepository');
const slugify = require('slugify');
const sequelize = require('sequelize');
const { RecordNotFoundError } = require('../errors');
class ProductService {
  list = async (qs) => {
    const { name, category_id, page, page_size } = qs;
    let queryProducts = { status: 1 };
    if (name) {
      queryProducts = {
        ...queryProducts,
        name: sequelize.where(
          sequelize.fn('LOWER', sequelize.col('name')),
          'LIKE',
          '%' + name.toLowerCase() + '%'
        )
      };
    }
    if (category_id) {
      queryProducts = { ...queryProducts, category_id: category_id };
    }

    const product = await ProductRepository.findAll({ query: queryProducts, page, page_size });
    return product;
  };

  count = async (qs) => {
    const { name, category_id } = qs;

    let queryProducts = { status: 1 };
    if (name) {
      queryProducts = {
        ...queryProducts,
        name: {
          [Op.iLike]: `%${name}%`
        }
      };
    }
    if (category_id) {
      queryProducts = { ...queryProducts, category_id: category_id };
    }

    const product = await ProductRepository.count({ query: queryProducts });
    return product;
  };

  delete = async (id) => {
    const result = await ProductRepository.delete(id);
    return result;
  };

  edit = async (id, newData) => {
    const product = await ProductRepository.edit(id, newData);
    const ProductImage = await ProductImageRepository.deleteAll(id);
    if (newData.images) {
      newData.images.forEach(async (url) => {
        await ProductImageRepository.add(id, url);
      });
    }

    return product;
  };

  editData = async (id, newData) => {
    const product = await ProductRepository.edit(id, newData);
    return product;
  };

  create = async (id, reqBody) => {
    const slug = slugify(reqBody.name, {
      lower: true
    });
    const product = await ProductRepository.create(id, reqBody, slug);
    const product_id = product.id;
    const uploadImage = await reqBody.images.forEach(async (url) => {
      await ProductImageRepository.add(product_id, url);
    });
    return product;
  };

  bySlug = async (slug, response) => {
    const product = await ProductRepository.getBySlug(slug);
    if (!product) {
      const err = new RecordNotFoundError(slug);
      return response.status(404).json(err);
    }
    return product;
  };

  byOwner = async (id) => {
    const product = await ProductRepository.getNotSoldByOwner(id);
    return product;
  };

  byOwnerCount = async (id) => {
    const product = await ProductRepository.getProductCountByOwner(id);
    return product;
  };

  soldByOwner = async (id) => {
    const product = await ProductRepository.getSoldByOwner(id);
    return product;
  };

  notSoldByOwner = async (id) => {
    const product = await ProductRepository.getNotSoldByOwner(id);
    return product;
  };

  byOwnerAndId = async (user_id, id, response) => {
    const product = await ProductRepository.getByOwnerAndId(user_id, id);
    if (!product) {
      const err = new RecordNotFoundError(id);
      return response.status(404).json(err);
    }
    return product;
  };

  likeProduct = async (user_id, product_id, response) => {
    const like = await ProductLikeRepository.getByUserAndProduct(user_id, product_id);
    if (!like) {
      await ProductLikeRepository.add(user_id, product_id);
      return 'success like product';
    } else {
      await ProductLikeRepository.delete(like.id);
      return 'success unlike product';
    }
  };

  checkLikeStatus = async (user_id, product_id, response) => {
    const status = await ProductLikeRepository.checkStatus(user_id, product_id);
    if (status) {
      return 1;
    } else {
      return 0;
    }
  };

  getProductLiked = async (user_id) => {
    const product = await ProductLikeRepository.getByUser(user_id);
    return product;
  };
}

module.exports = new ProductService();
