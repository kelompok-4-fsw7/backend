const ApplicationError = require('./ApplicationError');

class EmailRegisteredError extends ApplicationError {
  constructor(email) {
    super(`${email} is already active`);
  }
}

module.exports = EmailRegisteredError;
