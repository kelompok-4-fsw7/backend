const ApplicationError = require('./ApplicationError');

class TokenInvalidError extends ApplicationError {
  constructor() {
    super(`Token invalid`);
  }
}

module.exports = TokenInvalidError;
