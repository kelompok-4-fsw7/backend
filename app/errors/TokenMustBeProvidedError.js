const ApplicationError = require('./ApplicationError');

class TokenMustBeProvidedError extends ApplicationError {
  constructor() {
    super(`Token must be provided`);
  }
}

module.exports = TokenMustBeProvidedError;
