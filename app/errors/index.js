const EmailNotRegisteredError = require('./EmailNotRegisteredError');
const WrongPasswordError = require('./WrongPasswordError');
const EmailAlreadyTakenError = require('./EmailAlreadyTakenError');
const TokenInvalidError = require('./TokenInvalidError');
const TokenMustBeProvidedError = require('./TokenMustBeProvidedError');
const RecordNotFoundError = require('./RecordNotFoundError');
const UserInactive = require('./UserInactive');
const UserAlreadyActiveError = require('./UserAlreadyActiveError');
const UnprocessableEntityError = require('./UnprocessableEntityError');

module.exports = {
  UserInactive,
  EmailNotRegisteredError,
  EmailAlreadyTakenError,
  WrongPasswordError,
  TokenInvalidError,
  TokenMustBeProvidedError,
  RecordNotFoundError,
  UserAlreadyActiveError,
  UnprocessableEntityError
};
