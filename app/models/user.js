'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsTo(models.City, {
        foreignKey: 'city_id',
        as: 'city'
      });
    }
  }
  User.init(
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      image: DataTypes.STRING,
      city_id: DataTypes.INTEGER,
      address: DataTypes.STRING,
      phone: DataTypes.STRING,
      status: DataTypes.INTEGER,
      deleted_at: DataTypes.DATE,
      token: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'User',
      underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at'
    }
  );
  return User;
};
