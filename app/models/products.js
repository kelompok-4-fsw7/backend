'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      products.belongsTo(models.Category, {
        foreignKey: 'category_id',
        as: 'category'
      });
      products.hasMany(models.ProductImage, {
        sourceKey: 'id',
        foreignKey: 'product_id',
        as: 'images'
      });
      products.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'user',
        password: 'SET NULL'
      });
    }
  }
  products.init(
    {
      category_id: DataTypes.INTEGER,
      user_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      price: DataTypes.INTEGER,
      status: DataTypes.INTEGER,
      description: DataTypes.STRING,
      deleted_at: DataTypes.DATE,
      slug: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'Products',
      underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
      paranoid: true
    }
  );
  return products;
};
