const ApplicationController = require('./ApplicationController');
const ProductService = require('../services/productService');
const NotificationService = require('../services/notificationService');
const {
  SuccessFetchResponse,
  SuccessCreateResponse,
  SuccessDeleteResponse,
  SuccessFetchMetaResponse
} = require('../libs/response');

class ProductController extends ApplicationController {
  handleListProduct = async (req, res, next) => {
    let product = [];
    let productCount = 0;
    let pagination = [];

    const page = req.query?.page || '1';
    const pageSize = req.query?.page_size || '10';
    const qs = {
      ...req.query,
      page: +page,
      page_size: +pageSize
    };

    product = await ProductService.list(qs);
    productCount = await ProductService.count(qs);
    pagination = this.buildPaginationObject(req, productCount);

    return SuccessFetchMetaResponse(res, product, pagination);
  };

  handleDeleteProduct = async (req, res) => {
    const id = req.params.id;
    const result = await ProductService.delete(id);
    return SuccessDeleteResponse(res, result);
  };

  handleProduct = async (req, res, next) => {
    let product = null;
    const slug = req.params.slug;
    try {
      product = await ProductService.bySlug(slug, res);
      return SuccessFetchResponse(res, product);
    } catch (err) {
      console.log(err.message);
      next(err);
    }
  };

  handleCreateProducts = async (req, res) => {
    const id = req.user.id;
    const reqBody = req.body;
    const product = await ProductService.create(id, reqBody);
    if (reqBody?.status == 1) {
      await NotificationService.createProduct(id, product.id);
    }
    return SuccessCreateResponse(res, 'Product created successfully', product);
  };

  handleEditProduct = async (req, res) => {
    const id = req.params.id;
    const newData = req.body;

    const product = await ProductService.edit(id, newData);
    if (newData?.status == 1) {
      await NotificationService.createProduct(req.user.id, req.params.id);
    }
    return SuccessFetchResponse(res, product);
  };

  handleProductByOwner = async (req, res) => {
    const id = req.user.id;
    let product = [];
    product = await ProductService.byOwner(id);
    return SuccessFetchResponse(res, product);
  };

  handleProductByOwnerAndId = async (req, res, next) => {
    const user_id = req.user.id;
    const product_id = req.params.id;
    let product = null;
    try {
      product = await ProductService.byOwnerAndId(user_id, product_id, res);
      return SuccessFetchResponse(res, product);
    } catch (e) {
      console.log(e.message);
      next(e);
    }
  };

  handleProductLikes = async (req, res) => {
    const user_id = req.user.id;
    const product_id = req.params.id;
    let like = null;

    like = await ProductService.likeProduct(user_id, product_id, res);
    return SuccessFetchResponse(res, like);
  };

  handleProductLikeStatus = async (req, res) => {
    const user_id = req.user.id;
    const product_id = req.params.id;
    const like = await ProductService.checkLikeStatus(user_id, product_id);
    return SuccessFetchResponse(res, like);
  };

  handleProductsLiked = async (req, res) => {
    const user_id = req.user.id;
    const like = await ProductService.getProductLiked(user_id);
    return SuccessFetchResponse(res, like);
  };

  handleProductSoldByOwner = async (req, res) => {
    const id = req.user.id;
    let product = [];
    product = await ProductService.soldByOwner(id);
    return SuccessFetchResponse(res, product);
  };
  handleProductNotSoldByOwner = async (req, res) => {
    const id = req.user.id;
    let product = [];
    product = await ProductService.notSoldByOwner(id);
    return SuccessFetchResponse(res, product);
  };

  handleProductCountByOwner = async (req, res) => {
    const id = req.user.id;
    let product = [];
    product = await ProductService.byOwnerCount(id);
    return SuccessFetchResponse(res, product);
  };
}

module.exports = ProductController;
