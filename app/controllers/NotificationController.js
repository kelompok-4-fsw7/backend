const ApplicationController = require('./ApplicationController');
const NotificationService = require('../services/notificationService');
const { SuccessFetchMetaResponse, SuccessUpdateResponse } = require('../libs/response');

class NotificationController extends ApplicationController {
  handleGetNotificationByUserID = async (req, res) => {
    const qs = {
      user_id: req.user.id,
      page: +(req.query?.page || '1'),
      page_size: +(req.query?.page_size || '5')
    };
    const notification = await NotificationService.getByUserId(qs);
    const notificationCount = await NotificationService.count(qs);
    const pagination = this.buildPaginationObject(req, notificationCount);

    return SuccessFetchMetaResponse(res, notification, pagination);
  };

  handleUpdateRead = async (req, res, next) => {
    const id = req.params.id;
    try {
      await NotificationService.updateRead(id, res);
      return SuccessUpdateResponse(res, null, 'Success update notification');
    } catch (err) {
      next(err);
    }
  };
}

module.exports = NotificationController;
