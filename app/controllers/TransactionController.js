const ApplicationController = require('./ApplicationController');
const TransactionService = require('../services/transactionService');
const { SuccessFetchResponse } = require('../libs/response');

class TransactionController extends ApplicationController {
  handleListTransactionBuyer = async (req, res) => {
    const user_id = req.user.id;
    const transaction = await TransactionService.getTransactionByBuyerID(user_id);
    return SuccessFetchResponse(res, transaction);
  };

  handleListTransactionSeller = async (req, res) => {
    const user_id = req.user.id;
    const transaction = await TransactionService.getTransactionBySellerID(user_id);
    return SuccessFetchResponse(res, transaction);
  };
}
module.exports = TransactionController;
