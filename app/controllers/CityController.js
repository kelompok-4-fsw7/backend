const ApplicationController = require('./ApplicationController');
const CityService = require('../services/cityService');
const { SuccessFetchResponse } = require('../libs/response');

class CityController extends ApplicationController {
  handleListCity = async (req, res) => {
    const city = await CityService.list();
    return SuccessFetchResponse(res, city);
  };
}

module.exports = CityController;
