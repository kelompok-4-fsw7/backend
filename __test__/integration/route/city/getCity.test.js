const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Get city', () => {
  it('Success get city', (done) => {
    request(app)
      .get('/v1/cities')
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
