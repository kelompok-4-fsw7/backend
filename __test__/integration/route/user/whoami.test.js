const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Get user by token', () => {
  let token = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        done();
      })
      .catch(done);
  });

  it('Success get user by token', (done) => {
    request(app)
      .get('/v1/auth/whoami')
      .set('Authorization', `Bearer ${token}`)
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.email).toEqual(userCredential.email);
        done();
      })
      .catch(done);
  });
});
