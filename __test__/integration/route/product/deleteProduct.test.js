const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { Products } = require('../../../../app/models');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('delete product', () => {
  let product = null;
  let token = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeEach(async () => {
    product = await Products.findOne({
      where: {
        slug: 'jam-tangan-rolex'
      }
    });
  });

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        done();
      })
      .catch(done);
  });

  afterAll(async () => {
    await Products.create({
      name: 'Jam Tangan Rolex',
      user_id: 2,
      category_id: 1,
      price: 2800000,
      status: 1,
      description:
        'No minus need to declare. Barang simpanan, jual santai cari harga baik. Milik pribadi dan jarang sekali dipakai.',
      slug: 'jam-tangan-rolex'
    });
  });

  it('success delete products', (done) => {
    product = product.dataValues.id;
    request(app)
      .delete(`/v1/products/${product}/delete`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .expect(202)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('token invalid', (done) => {
    product = '10';
    MockToken =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9eyJ1c2VyIjp7ImlkIjo1NSwibFtZSI6IlJha2hhIiwiZW1haWwiOiJyYWtoYUBiaW5hci5jby5pZCIsImltYWdlIjoiaHR0cDovL2xvcmVtZmxpY2tyLmNvbS82NDAvNDgwIiwiY2l0eV9pZCI6MSwiYWRkcmVzcyI6ImphbGFuLWphbGFuIiwicGhvbmUiOiIwODEyMzQ1NjciLCJzdGF0dXMiOjEsImRlbGV0ZWRfYXQiOm51bGwsInRva2VuIjpudWxsLCJjcmVhdGVkX2F0IjoiMjAyMi0wNy0xOFQxMzoyMTo1Ny42MjZaIiwidXBkYXRlZF9hdCI6IjIwMjItMDctMThUMTM6MjE6NTcuNjI2WiJ9LCJpYXQiOjE2NTgxNTA1MzJ9.R2n1fnFMZXl0f2j2EUrzneXpv8IdC5KcE8l-vu52UFo';
    request(app)
      .delete(`/v1/products/${product}/delete`)
      .set('Authorization', `Bearer ${MockToken}`)
      .set('Accept', 'application/json')
      .expect(400)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('token must be provided', (done) => {
    product = '10';
    MockToken = '';
    request(app)
      .delete(`/v1/products/${product}/delete`)
      .set('Authorization', `Bearer ${MockToken}`)
      .set('Accept', 'application/json')
      .expect(401)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
