const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('get notifications by id', () => {
  let token = null;
  let id = 1;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then(async (res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        done();
      })
      .catch(done);
  });

  it('handle update read', (done) => {
    request(app)
      .put(`/v1/notification/${id}`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .expect(202)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('fail  update read', (done) => {
    id = true;
    request(app)
      .put(`/v1/notification/${id}`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .expect(404)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
