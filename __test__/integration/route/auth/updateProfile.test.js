const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { User } = require('../../../../app/models');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

let token = null;
const userCredential = {
  email: 'al@mailinator.com',
  password: '1234567'
};

const updateUserData = {
  phone: '081213141516',
  address: 'jalan kenangan',
  image: 'www.google.com',
  city_id: '5'
};
const updateUserDataFail = {
  phone: '081213141516',
  address: 'jalan kenangan',
  image: 'www.google.com',
  city_id: 'abcd',
  password: 'active'
};

describe('Update Profil test', () => {
  afterAll(async () => {
    await User.update(
      {
        phone: '081234567',
        address: 'jalan-jalan',
        // city_id: '1',
        image: 'http://loremflickr.com/640/480'
      },
      {
        where: {
          name: 'Al',
          email: 'al@mailinator.com'
        }
      }
    );
  });

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        done();
      })
      .catch(done);
  });

  it('Success update user profile', (done) => {
    request(app)
      .put('/v1/auth/profile')
      .set('Authorization', `Bearer ${token}`)
      .send(updateUserData)
      .expect(202)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('Fail update user profile', (done) => {
    request(app)
      .put('/v1/auth/profile')
      .set('Authorization', `Bearer ${token}`)
      .set('content-type', 'application/json')
      .send(updateUserDataFail)
      .expect(422)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
