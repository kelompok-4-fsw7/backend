const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { Products, Offers } = require('../../../../app/models');
const { decodeToken } = require('../../../../app/libs/token');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('get offer by seller', () => {
  let offer_id = 1;
  let product_id = null;
  let token = null;
  let user_id = null;
  let mockOfferSuccess = null;
  let mockOfferReject = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeEach(async () => {
    let product = await Products.findOne({
      where: {
        slug: 'yamaha-vixion-2014'
      }
    });
    product_id = product.dataValues.id;
    mockOfferSuccess = await Offers.create({
      product_id,
      user_id,
      offer_price: 20000,
      status: 2,
      seller_id: user_id + 1
    });
    mockOfferReject = await Offers.create({
      product_id,
      user_id,
      offer_price: 20000,
      status: 0,
      seller_id: user_id + 1
    });
  });

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then(async (res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        user = decodeToken(token);
        user_id = user.user.id;
        done();
      })
      .catch(done);
  });

  it('handle update status', (done) => {
    request(app)
      .put(`/v1/product/${offer_id}/offer`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .expect(202)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('handle update status reject', (done) => {
    offer_id = mockOfferReject.dataValues.id;
    request(app)
      .put(`/v1/product/${offer_id}/offer`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send({
        status: 0
      })
      .expect(202)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('handle update status success', (done) => {
    offer_id = mockOfferSuccess.dataValues.id;
    request(app)
      .put(`/v1/product/${offer_id}/offer`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send({
        status: 2
      })
      .expect(202)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
