const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { Products, Offers } = require('../../../../app/models');
const { decodeToken } = require('../../../../app/libs/token');

jest.setTimeout(30000);
app.use(express.json());
router.apply(app);

describe('update products status', () => {
  let offer_id = null;
  let product_id = null;
  let token = null;
  let user_id = null;
  let mockOffer = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeEach(async () => {
    let product = await Products.findOne({
      where: {
        slug: 'yamaha-vixion-2014'
      }
    });
    product_id = product.dataValues.id;

    mockOffer = await Offers.create({
      product_id,
      user_id,
      offer_price: 20000,
      status: 1,
      seller_id: user_id + 1
    });
  });

  afterAll(async () => {
    await Offers.destroy({
      where: {
        id: offer_id
      }
    });
  });

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then(async (res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        user = decodeToken(token);
        user_id = user.user.id;
        done();
      })
      .catch(done);
  });

  it('success update products status accepted', (done) => {
    offer_id = mockOffer.dataValues.id;
    request(app)
      .put(`/v1/product/${product_id}/offer/${offer_id}`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send({
        status: 2
      })
      .expect(202)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('success update products status rejected', (done) => {
    offer_id = mockOffer.dataValues.id;
    request(app)
      .put(`/v1/product/${product_id}/offer/${offer_id}`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send({
        status: 0
      })
      .expect(202)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
