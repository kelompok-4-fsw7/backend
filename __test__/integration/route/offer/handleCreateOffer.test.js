const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { Products, Offers } = require('../../../../app/models');
const { decodeToken } = require('../../../../app/libs/token');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('get offer by seller', () => {
  let token = null;
  let product_id = null;
  let dataProduct = null;
  let offer_id = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then(async (res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;

        const data = decodeToken(token);
        const user_id = data.user.id;

        await Products.create({
          name: 'Motor Baru',
          price: 100000,
          user_id: user_id,
          category_id: 1,
          description: 'Motor baru nih',
          images: ['https://google.com']
        });

        dataProduct = await Products.findOne({
          where: {
            user_id
          }
        });
        done();
      })
      .catch(done);
  });

  afterAll(async () => {
    await Offers.destroy({
      where: {
        id: offer_id
      }
    });
  });

  it('handle create offer', (done) => {
    product_id = dataProduct.dataValues.id;
    request(app)
      .post(`/v1/product/${product_id}/offer`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send({
        offer_price: 30000000
      })
      .expect(201)
      .then((res) => {
        offer_id = res.body.data.id;
        done();
      })
      .catch(done);
  });
});
